#!/bin/bash
[ -z $AB_WEBSERVER_ADDR ] && AB_WEBSERVER_ADDR=$AB_CONTROLLER_ADDR
init-dirs.sh > /var/log/startup.log
re='^(0*(1?[0-9]{1,2}|2([0-4][0-9]|5[0-5]))\.){3}'
re+='0*(1?[0-9]{1,2}|2([‌​0-4][0-9]|5[0-5]))$'

if [[ $AB_CONTROLLER_ADDR  =~ $re ]]; then
	   grep ds-autobuilder-controller /etc/hosts || echo $AB_CONTROLLER_ADDR ds-autobuilder-controller >> /etc/hosts
fi
if [[ $AB_WEBSERVER_ADDR =~ $re ]]; then
	   grep dsautobuilder.internal /etc/hosts || echo $AB_WEBSERVER_ADDR dsautobuilder.internal >> /etc/hosts
fi

case $AB_MODE in	
	controller)
		echo starting in controller mode >> /var/log/startup.log
		supervisord -c /etc/supervisor-controller.conf
		;;
	controller-worker)
		echo starting in controller and worker mode >> /var/log/startup.log
		supervisord -c /etc/supervisor.conf
		;;
	worker)
		echo starting in worker mode >> /var/log/startup.log
		supervisord -c /etc/supervisor-worker.conf
		;;
esac
exit 0
